﻿<%@ Page Title="Cessions | Tana" Language="C#" MasterPageFile="FrenchMasterPage.master" AutoEventWireup="true" CodeFile="Investments.aspx.cs" Inherits="Pages_Investments" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>
            SetMenu = "Investments";
        </script>
        <meta name="description" content=" Investments with which Tana Africa works with" />
        <meta name="keywords" content="Promisador Nigeria, Promasidor Group, affordable, small sachets, powdered milk, food seasoning and tea brands" />
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <span class="breadCrumb">You are Here : <a href="../Default.aspx">Accueil</a> > Cessions</span>
        <br />
        <br />
        <h1><strong>CESSIONS</strong></h1><br/><br/>
		<h2>Participation minoritaire au sein du Groupe Promasidor</h2>
        <p align="justify"><strong>Présentation de Promasidor</strong>: Promasidor (<a href="http://www.promasidor.com" target="_blank">www.promasidor.com</a>) est un groupe agro-alimentaire panafricain avec des sites industriels dans neuf pays africains et commercialisant ses produits dans une trentaine de pays africains. Promasidor dispose d’un large portefeuille de marques couvrant plusieurs catégories de produits: lait en poudre, lait en poudre aromatisé, ingrédients culinaires, céréales et thé. Leader en matière d’innovation, Promasidor a été précurseur sur le lait en poudre en sachet, les ingrédients culinaires sous forme de sachet et le thé en sac circulaire.</p>

        <p align="center"><img src="/Images/promasidor.jpg" alt="promasidor" title="promasidor" width="400"  /></p><br />
		
		<h2>Participation minoritaire au sein de la Société d’Investissement pour l’Agriculture Tropicale (S.I.A.T)</h2>
        <p align="justify"><strong>Présentation de SIAT</strong>: SIAT (<a href="http://www.siat-group.com" target="_blank">www.siat-group.com</a>) détient plus de 70 000 hectares de palmeraies et de plantations d’hévéa en Afrique de l’Ouest et Centrale : Gabon (palmeraies, plantations d’hévéa et élevage bovin) ; Côte d’Ivoire (plantations d’hévéa) ; Nigéria (palmeraies) et Ghana (palmeraies). SIAT possède également des usines et raffineries d’huile de palme adjacentes à chaque palmeraie. L’activité de SIAT porte aussi sur l'élevage du bétail, la production et la distribution d'huiles consommables et la production de savon au Gabon. Le siège social de SIAT est situé à Bruxelles en Belgique.</p>

        <p align="center"><img src="/Images/siat-1.jpg" alt="siat-1.jpg" title="siat-1.jpg" width="200"  />
		<img src="/Images/siat-2.jpg" alt="siat-2" title="siat-2" width="200"  />
		<img src="/Images/siat-3.jpg" alt="siat-3.jpg" title="siat-3.jpg" width="200"  /></p><br /><br/>
		
		



    </asp:Content>