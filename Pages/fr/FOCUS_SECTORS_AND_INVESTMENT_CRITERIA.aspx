﻿<%@ Page Title="Principaux secteurs et critères d'investissement | Tana" Language="C#" MasterPageFile="FrenchMasterPage.master" AutoEventWireup="true" CodeFile="FOCUS_SECTORS_AND_INVESTMENT_CRITERIA.aspx.cs" Inherits="Pages_FOCUS_SECTORS_AND_INVESTMENT_CRITERIA" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>
            SetMenu = "About";
        </script>
        <meta name="description" content="Tana invests between US$20 million and US$75 million to acquire significant minority or control equity positions in established businesses across Africa that operate within the Consumer and Agriculture sectors." />
        <meta name="keywords" content="Consumer Focus, Agriculture Focus, Investment Criteria,Food, beverage and personal care fast moving consumer goods" />

    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <span class="breadCrumb">You are Here : <a href="../Default.aspx">Accueil</a> > À Propos  > Principaux secteurs et critères d'investissement</span>
        <br />
        <br />
        <h1><strong>SECTEURS CIBLES ET CRITERES D’INVESTISSEMENT</strong></h1>
        <p align="justify">Tana investit des tickets compris entre US$20 millions et US$75 millions pour prendre des participations minoritaires ou majoritaires dans des entreprises bien établies.</p>

        <h2>Focus sur la Consommation</h2>
        <p align="justify">Tana se concentre sur des entreprises bien positionnées pour répondre aux besoins de consommation de la population jeune, dynamique et croissante du continent africain.</p>

        <p><strong>Tana privilégie les secteurs d’activité suivants :</strong></p>
        <ul>
            <li>Biens de grande consommation (alimentaire et non alimentaire)</li>
			<li>Commerce de détail</li>
			<li>Education</li>
			<li>Santé</li>
			<li>Agro-industrie</li>
        </ul>
        <p align="justify">Tana envisagerait également des investissements de manière plus sélective dans les secteurs des loisirs, de l’industrie manufacturière, des services financiers, des médias, de la logistique et des services aux entreprises.</p>

        <!--<p align="center"><br />
  <img src="../Images/agriculture-focus.jpg" alt="Agriculture Focus" title="Agriculture Focus" width="438" height="213" /></p>
   -->
        <h2>Critères d’Investissement</h2>
        <p align="justify"><strong>Tana souhaite investir dans les entreprises opérant en Afrique et possédant les caractéristiques suivantes:</strong></p>
        <ul>
            <li>Marché de grande taille, à forte croissance et à faible pénétration</li>
            <li>Dépendance minimale vis-à-vis du secteur public</li>
            <li>Secteur d’activité attractif avec un « business model » bien établi, profitable et viable</li>
            <li>« Business model » exportable et fondé sur des processus de fabrication et/ou de distribution standardisés et susceptibles d’être mis en œuvre sur d’autres marchés</li>
            <li>Opportunités des croissances organique et externe (par le biais d’acquisitions)</li>
            <li>Avantage compétitif distinct permettant de maintenir une position concurrentielle forte sur le marché</li>
            <li>Entrepreneur/actionnaire majoritaire/équipe dirigeante ayant des compétences opérationnelles de premier plan et une vision stratégique claire</li>
            <li>Existence de leviers de création de valeur clairement identifiables et actionnables</li>
            <li>Pratiques saines de bonne gouvernance</li>
        </ul>
        <br />
    </asp:Content>