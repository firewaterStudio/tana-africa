﻿<%@ Page Title="Portefeuille | Tana" Language="C#" MasterPageFile="FrenchMasterPage.master" AutoEventWireup="true" CodeFile="Investments.aspx.cs" Inherits="Pages_Investments" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>
            SetMenu = "Investments";
        </script>
        <meta name="description" content=" Investments with which Tana Africa works with" />
        <meta name="keywords" content="Promisador Nigeria, Promasidor Group, affordable, small sachets, powdered milk, food seasoning and tea brands" />
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <span class="breadCrumb">You are Here : <a href="../Default.aspx">Accueil</a> > Portefeuille</span>
        <br />
        <br />
        <h1><strong>PARTICIPATIONS ACTUELLES</strong></h1><br/>
        <br />

        <h2>Participation minoritaire au sein de KITEA S.A. (KITEA)</h2>
        <p align="justify"><strong>Présentation de KITEA : </strong>KITEA (<a href="http://www.kitea.ma">www.kitea.ma</a>) est le leader marocain dans la distribution de meubles de maison, de meubles de bureau et d’accessoires de maison présent dans 17 villes du Maroc avec plus de 20 points de ventes et 2 magasins en Afrique subsaharienne (Guinée Equatoriale et République Démocratique du Congo). La société a été fondée en 1993 avec l’ouverture d’un premier magasin à Route d’El Jadida à Casablanca sur une superficie de 300 m² et 5 employés. KITEA dispose aujourd’hui d’une surface d’exposition de plus de 50 000 m² sous sa marque propre et la franchise CASA, et emploie plus de 1 000 personnes. L’enseigne KITEA a réalisé son expansion sur le territoire national, en se basant dans un premier temps sur une stratégie de développement en partenariat avec les enseignes de grande distribution, avant de s’orienter vers la réalisation depuis 2008 de ses propres centres commerciaux “KITEA Géants”.</p>
        <p align="center">
            <img src="/Images/kitea-3.png" alt="kitea-3" title="kitea-3" width="800" /></p>
        <br />
        <br />

        <h2>Participation minoritaire au sein de Regina for Pasta and Food Industries S.A.E. (Regina)</h2>
        <p align="justify"><strong>Présentation de Regina</strong>: Regina (<a href="http://www.pastaregina.com" target="_blank">www.pastaregina.com</a>) est un groupe agro-alimentaire égyptien créé en 1986 et qui est actuellement le deuxième producteur de pâtes alimentaires en Egypte. Regina domine le segment haut de gamme des pâtes à base de semoule (blé dur) avec les marques Regina et Masreya. Regina a également un positionnement fort sur le segment plus populaire des pâtes à blé́ mou sous les marques Star et King. Regina dispose d’un potentiel de croissance significatif pour ses ventes à l’exportation, en particulier vers les marchés du Moyen-Orient et d’Afrique sub-saharienne.</p>

        <p align="center"><img src="/Images/regina.jpg" alt="regina" title="regina" width="438" /></p><br />
        
        <br />

        <h2>Participation minoritaire au sein de la Société Alimentaire de la Nomba (SAN)</h2>
        <p align="justify"><strong>Présentation de SAN: </strong>Créé en 1979, SAN (<a href="http://www.san-gel.com">www.san-gel.com</a>) est le premier importateur et distributeur de produits alimentaires surgelés au Gabon. SAN importe, transforme, reconditionne et distribue ses produits à travers un réseau de clients grossistes et demi- grossistes, ainsi qu'au sein de magasins de libre-service et commerce de détail (« freezer centers » sous l’enseigne SAN Gel) stratégiquement positionnés à Libreville. SAN occupe une position dominante dans la distribution des produits surgelés suivants : volaille, viande porcine et bovine, poissons, légumes, plats préparés, glaces et desserts. SAN bénéficie de fortes perspectives de croissance au Gabon et au-delà dans les pays voisins.</p>
        <p align="center"><img src="/Images/nomba-1.jpg" alt="nomba-1" title="nomba-1" width="310" />
		<img src="/Images/nomba-2.jpg" alt="nomba-2" title="nomba-2" width="250" />
		<img src="/Images/nomba-3.jpg" alt="nomba-3" title="nomba-3" width="250" /></p>
        <br />
        <br />


        
        <h2>Participation minoritaire au sein de Sana Education (Sana)</h2>
        <p align="justify"><strong>Présentation de Sana: </strong>Sana Education (<a href="http://www.sanaeducation.com" target="_blank">www.sanaeducation.com</a>) aspire à être le groupe scolaire panafricain de référence offrant une éducation primaire et secondaire privée d’excellente qualité à travers le continent africain. En matière d’éducation, Sana a une approche différenciée qui garantit des standards de qualité internationaux sans pour autant compromettre sur les particularismes locaux liés à l’ancrage dans les cultures africaines. Sana a pour objectif de former des individus intellectuellement brillants, émotionnellement épanouis et culturellement enracinés qui seront les instigateurs d'une nouvelle ère d'excellence africaine au 21e siècle. Le groupe Sana inclut dans son portefeuille des écoles offrant aussi bien des programmes académiques nationaux qu’internationaux. Il cible à la fois les enfants issus des milieux aisés que les enfants issus de familles de classe moyenne. Sana aspire à̀ devenir un groupe scolaire panafricain regroupant 100 000 étudiants. Bien qu'initialement focalisé sur le Maroc, le groupe étendra sa présence sur le continent africain au fil du temps. Sana a été créée en juillet 2015 par joint-venture entre le Groupe Saham et Tana Africa Capital.</p>

        <p align="center"><img src="/Images/Sana-tana.jpg" alt="sana-tana" title="sana-tana" width="438" /></p>
		
		



    </asp:Content>