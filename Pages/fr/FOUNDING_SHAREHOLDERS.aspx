﻿<%@ Page Title="Actionnaires Fondateurs | Tana" Language="C#" MasterPageFile="FrenchMasterPage.master" AutoEventWireup="true" CodeFile="Team.aspx.cs" Inherits="Pages_Team" %>

  <asp:Content ID="Content3" ContentPlaceHolderID="head" Runat="Server">
    <script>
      SetMenu = "About";
    </script>
    <meta name="description" content="History of the founding shareholders who began and started Tana Africa" />
    <meta name="keywords" content="E. Oppenheimer & Son, Anglo American, De Beers, Temasek, inancial services; transportation, logistics and industrials, telecommunications, media & technology, life sciences, energy and resources."
    />
  </asp:Content>
  <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <span class="breadCrumb">You are Here :
      <a href="../Default.aspx">Accueil</a> > À Propos > Actionnaires Fondateurs</span>
    <br />
    <br />
    <h1>
      <strong>ACTIONNAIRES FONDATEURS</strong>
    </h1>
    <p align="justify">Tana Africa Capital a été créée en 2011 par joint-venture à 50/50 entre E. Oppenheimer & Son et Temasek Holdings. Les
      deux actionnaires fondateurs partagent une ambition commune de contribuer à la création de valeur sur le continent
      africain par le biais d’un apport capitalistique et d’une assistance stratégique. À ce jour, Tana a investi plus de
      US$250 millions à travers le continent africain et a finalisé en 2017 la levée de US$300 millions supplémentaires auprès
      de ses actionnaires historiques.</p>

    <h2>E. Oppenheimer &amp; Son</h2>
    <p align="justify">E. Oppenheimer & Son (“EOS”) est la holding d’investissement de la famille Oppenheimer, fondatrice du groupe minier international
      Anglo American (
      <a href="http://www.angloamerican.com" target="_blank">www.angloamerican.com</a>), et jusqu’en 2012, actionnaire de référence de De Beers (
      <a href="http://www.debeersgroup.com" target="_blank">www.debeersgroup.com</a>), le leader mondial du diamant. EOS détient un vaste portefeuille d’investissements incluant
      des participations dans des entreprises opérant en Afrique, en Europe, en Amérique du Nord et en Asie. EOS possède
      notamment des participations dans des entreprises sud-africaines aux côtés d’entrepreneurs et de fonds d’investissement
      locaux.
      <br />
      <br/> EOS détient également deux autres structures spécialisées respectivement dans les investissements en projets d’infrastructures
      en Afrique et les investissements dans des sociétés cotées en bourse en Afrique, en complément de Tana Africa Capital
      dont le mandat exclusif est d’investir dans des entreprises privées opérant en Afrique.
      <br />
      <br/> En sus de ses participations financières, la famille Oppenheimer a créé The Brenthurst Foundation (
      <a href="http://www.thebrenthurstfoundation.org" target="_blank">www.thebrenthurstfoundation.org</a>), un « think-tank » basé à Johannesburg et travaillant de concert avec les gouvernements
      africains sur des problématiques de développement du continent. La fondation se spécialise dans l’identification et
      la mise en œuvre des bonnes pratiques de gouvernance en s’inspirant du modèle des économies à forte croissance. La
      fondation a également été sollicitée pour conseiller les gouvernements de nombreux pays africains (Rwanda, Libéria,
      Mozambique, Lesotho, Kenya, Swaziland et Zimbabwe).
      <br/>
      <br/> L’intérêt de la famille Oppenheimer pour l’Afrique est bien enraciné et se nourrit de l’expérience accumulée depuis
      quatre générations par les membres de la famille dans le cadre de leurs investissements sur le continent. La famille
      a une compréhension profonde du monde des affaires en Afrique, et un engagement réel pour poursuivre le développement
      socio-économique du continent.</p>
    <!-- <p><strong>E. Oppenheimer &amp; Son’s notable investments include:</strong></p>
  
  <table width="94%" border="0" align="center" cellpadding="7" cellspacing="1" >
    <tr bgcolor="white">
      <td valign="top" ><h3>DeBeers Group</h3>        
      The DeBeers Group is the world’s leading diamond company, employing ~20,000 people on 5 continents with 85% of the workforce employed in Africa. EOS currently owns 40% of DeBeers, with Anglo American owning a further 45% of the company and the government of Botswana holding the remaining 15%</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top" ><h3>Anglo American</h3>        
      Anglo American Plc is one of the world’s largest mining and natural resource groups, with sales of $25b in 2009 and a total market capitalization of ~$36b. EOS currently owns 2.26% of the outstanding shares of Anglo American Plc, and Nicky Oppenheimer sits on the company’s board as a Non-Executive Director.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top" ><h3>Stockdale Street - London</h3>        
      SSC London invests in leading private equity, hedge funds and endowment funds in the United States, Europe and Asia.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top" ><h3>Stockdale Street - South Africa</h3>SSC South Africa invests directly in South African companies, often working alongside leading South African corporations and private equity firms. </td>
    </tr>
    <tr bgcolor="white">
      <td valign="top" ><h3>Brenthurst - Foundation</h3>
      The Oppenheimer Family founded the Brenthurst Foundation in April 2005 as a think-tank to contribute to the debate around strategies and policies for strengthening Africa’s economic performance. The Brenthurst Foundation has served as a trusted advisor to senior government officials in a number of African countries, including Rwanda, Liberia, Mozambique, Lesotho, Kenya, Swaziland and Zimbabwe.</td>
    </tr>
  </table>
  <br /> -->
    <h2>Temasek </h2>
    <p align="justify">
      Créée en 1974, Temasek Holdings (&quot;Temasek&quot;) est une holding d&#39;investissement basée à Singapour (<a href="http://www.temasek.com.sg" target="_blank">www.temasek.com.sg</a>). Avec 11 bureaux à travers le monde, Temasek détient un portefeuille d’investissement diversifié
      d’une valeur estimée à S$308 milliards (c. US$235 milliards) au 31 mars 2018 et principalement concentré à Singapour
      et en Asie. Temasek a généré́ un retour sur investissement annuel de 15% en moyenne depuis sa création.
      <br/>
      <br/> Le portefeuille d'investissement de Temasek couvre un large éventail de secteurs, notamment les services financiers,
      le transport, la logistique, l’industrie, les télécommunications, les médias et la technologie, les sciences de la
      vie, les biens de grande consommation, l'immobilier, l'énergie et les ressources naturelles.
    </p>
    <!--<strong>Temasek’s notable investments include:</strong><br />
  </p>
  <table width="94%" border="0" align="center" cellpadding="7" cellspacing="1">
    <tr bgcolor="white">
      <td valign="top"><h3>Financial Services</h3>        
      Bank of China Limited, China Construction Bank Corporation, DBS Group Holdings, PT Bank Danamon Indonesia, Standard Chartered PLC, ICICI Bank Limited.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top"><h3>Telecommunications, Media &amp; Technology</h3>        Shin Corporation Public Company, Singapore Technologies Telemedia, STATS ChipPAC, Bharti Airtel, MediaCorp Pte. Ltd, Singapore Telecommunications Limited.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top"><h3>Transportation &amp; Industrials</h3>        Keppel Corporation, Neptune Orient Lines, PSA International Pte Ltd, Sembcorp Industries Ltd, Singapore Technologies, Singapore Airlines, Singapore Power, SMRT Corporation Ltd.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top"><h3>Life Sciences, Consumer &amp; Real Estate</h3>        Olam International Limited, CapitaLand Limited, Li &amp; Fung Limited, Mapletree Investments, SATS Ltd, Surbana Corporation, Wildlife Reserves.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top"><h3>Energy &amp; Resources</h3>        Chesapeake Energy, MEG Energy.</td>
    </tr>
  </table> -->

  </asp:Content>