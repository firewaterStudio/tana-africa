﻿<%@ Page Title="News - Tana" Language="C#" MasterPageFile="FrenchMasterPage.master" AutoEventWireup="true" CodeFile="News.aspx.cs" Inherits="Pages_News" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>
            SetMenu = "News";
        </script>
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <span class="breadCrumb">You are Here : <a href="../Default.aspx">Home</a> > News</span>
        <br />
        <br />
        <h1>News</h1>

        <table cellpadding="5" cellspacing="0" width="100%">
            <tr valign="top">
                <td width="36">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="NewsDate" width="36" height="36" align="center" valign="top"><span class="News1DateDay">31</span> <span class="News1DateMonth">Jan</span></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <div class="NewsTitle"><strong>Duis Autem Vel</strong></div>
                    <div class="NewsContent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam placerat libero vitae mi pretium et dictum ligula tempor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras id mauris
                        magna, vel gravida quam. Nullam tempus accumsan magna nec volutpat. Pellentesque euismod, justo quis gravida aliquet, justo urna vulputate massa, vestibulum commodo neque lectus nec sem. Sed hendrerit diam accumsan nibh faucibus
                        venenatis. Curabitur dignissim aliquet cursus. <a href="#">Read More</a></div>
                </td>
            </tr>
            <tr valign="top">
                <td width="36">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="NewsDate" width="36" height="36" align="center"><span class="News2DateDay">12</span> <span class="News2DateMonth">Jun</span></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <div class="NewsTitle"><strong>Duis Autem Vel</strong></div>
                    <div class="NewsContent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam placerat libero vitae mi pretium et dictum ligula tempor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras id mauris
                        magna, vel gravida quam. Nullam tempus accumsan magna nec volutpat. Pellentesque euismod, justo quis gravida aliquet, justo urna vulputate massa, vestibulum commodo neque lectus nec sem. Sed hendrerit diam accumsan nibh faucibus
                        venenatis. Curabitur dignissim aliquet cursus. <a href="#">Read More</a></div>
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
    </asp:Content>