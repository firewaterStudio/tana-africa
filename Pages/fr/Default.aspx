﻿<%@ Page Title="Bienvenue à Tana" Language="C#" MasterPageFile="FrenchMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Pages_Default" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>
            SetMenu = "Default";
        </script>
        <meta name="description" content="Tana Africa Capital is an Africa-focused investment company founded by E. Oppenheimer & Son International Ltd and Temasek."
        />
        <meta name="keywords" content="Africa-focused investment, African knowledge, Ethiopia’s Lake Tana, capital" />
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <table cellpadding=0 cellspacing=0 border="0">
            <tr valign=top>
                <td>
                    <h1>
                        <strong>BIENVENUE</strong>
                    </h1>
                    <p align="justify">Fondée par E. Oppenheimer &amp; Son et Temasek Holdings, Tana Africa Capital (‘’Tana”) est une holding
                        d’investissement focalisée sur l’Afrique où elle prend des participations minoritaires mais significatives
                        au capital d’entreprises privées opérant sur le continent. Par le biais d’un apport capitalistique
                        et d’une assistance stratégique, Tana souhaite contribuer à l’émergence d’entreprises africaines
                        de premier plan qui le resteront pour les générations à venir dans leurs domaines respectifs. Afin
                        d’atteindre cet objectif, Tana peut compter sur le prestige indéniable, l’expérience avérée et l’influence
                        certaine de ses actionnaires fondateurs en Afrique, ainsi que sur la connaissance du terrain et l’expertise
                        opérationnelle de son équipe dirigeante et de conseil. A l’image de son éponyme, le lac Tana, situé
                        en Ethiopie et source du Nil Bleu, Tana aspire à être une source de développement laissant une marque
                        indélébile, tangible et positive sur les entreprises qu’elle accompagne.</p>
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
    </asp:Content>