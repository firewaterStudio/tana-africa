﻿<%@ Page Title="Équipe | Tana" Language="C#" MasterPageFile="FrenchMasterPage.master" AutoEventWireup="true" CodeFile="Team.aspx.cs" Inherits="Pages_Team" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>
            SetMenu = "Team";
        </script>
        <meta name="description" content="List of staff who form the Tana team" />
        <meta name="description" content="Managing Director, Principal, Senior Associate, Finance Manager, Executive Assistant" />
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <span class="breadCrumb">You are Here :
            <a href="../Default.aspx">Accueil</a> > Équipe > Équipe Dirigeante</span>
        <br />
        <br />
        <h1>
            <strong>EQUIPE</strong>
        </h1>
        <p align="justify">Tana est gérée et conseillé par une équipe disposant d’une expérience significative et diverse dans l’investissement
            en capital risque/croissance en Afrique, le conseil en management, les fusions et acquisitions, la mise en œuvre
            opérationnelle de ces fusions et les services transactionnels.

        </p>
        <table width="100%" cellpadding="10" cellspacing="0" border="0">
            <tr>
                <td width="160">
                    <img src="/Images/team_duncan_randall.JPG" width="150" height="154" alt="Duncan Randall" title="Duncan Randall" /> </td>
                <td>
                    <h2>Duncan Randall
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Duncan a co-fondé Tana Africa Capital et est membre de son Comité d’Investissement. Duncan était précédemment
                        Associé Gérant chez Africa Holdings, fonds d’investissement captif d’E. Oppenheimer &amp; Sons. Avant
                        de rejoindre Africa Holdings, Duncan a fondé une société d’investissement en Afrique du Sud avec
                        une participation active à la conception, la levée de fonds et le lancement avec succès de deux fonds
                        d’investissement spécialisés dans l’apport de capital risque et de capital croissance. Il a également
                        été Consultant chez McKinsey au bureau de Johannesburg, participant à de nombreux projets dans plusieurs
                        secteurs d’activité, notamment dans le secteur des biens de grande consommation en Afrique sub-saharienne.
                        Duncan est diplômé de Oxford (Doctorat en Sciences Politiques) et de l’Université de Witwatersrand
                        (Maîtrise en Politiques Africaines).</p>
                </td>
            </tr>

            <!--<tr>
                <td width="160">
                    <img src="/Images/team_fabrice_ndjodo.JPG" width="150" height="154" alt="Fabrice Ndjodo" title="Fabrice Ndjodo" /> </td>
                <td>
                    <h2>Fabrice Ndjodo <span style="font-size:16px">– Directeur Général, Abidjan</span></h2>
                    <p align="justify">Fabrice occupait précédemment le poste de Directeur Exécutif à Liquid Africa, une société de conseil financier basée à Johannesburg et spécialisée en levée de fonds en Afrique sub-saharienne.  Avant de rejoindre Liquid Africa, Fabrice était Analyste Financier « sell-side » chez Macquarie First South Securities, où il a été classé 2e meilleur analyste pour conglomérats industriels en Afrique du Sud (couverture de 9 valeurs cotées sur la bourse de Johannesburg). Précédemment, il était Analyste Senior au sein de la division Corporate Finance de Macquarie (New York) et Analyste Financier à la Société Financière Internationale (Groupe Banque Mondiale, Washington DC) avec une implication dans différents projets en Europe, en Amérique du Sud et en Afrique. Fabrice est diplômé de Harvard Business School (MBA) et de HEC Paris (Finance et Management International).</p>
                </td>
            </tr>-->
            <tr>
                <td width="160">
                    <img src="/Images/Pieter.jpg" width="150" height="154" alt="Pieter Cilliers" title="Pieter Cilliers" /> </td>
                <td>
                    <h2>Pieter Cilliers
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Pieter est Directeur chez Tana Africa Capital. Pieter était précédemment Associé chez Bain &amp; Co. au
                        Brésil, ayant la responsabilité de nombreux projets dans les secteurs de biens de grande consommation,
                        de l'éducation et de la santé. Avant de rejoindre Bain &amp; Co, il était Chargé d’Affaires chez
                        Hancock Park Associates, un fonds d’investissement basé à Los Angeles et spécialisé dans l’apport
                        de capital croissance à des sociétés de taille moyenne. Précédemment, il a été Chargé de Mission
                        au sein de la division Fusions &amp; Acquisitions de Deloitte à San Francisco. Pieter est diplômé
                        de INSEAD (MBA) et de l’Université de Stellenbosch (Comptabilité). Il est également détenteur de
                        la certification internationale CFA (Chartered Financial Analyst) et est expert-comptable certifié
                        en Afrique du Sud.</p>
                </td>
            </tr>
            <tr>
                <td width="160">
                    <img src="/Images/team_ben_munda.jpg" width="150" height="154" alt="Ben Munda" title="Ben Munda" /> </td>
                <td>
                    <h2>Ben Munda
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">
                        Ben est Directeur chez Tana Africa Capital. Ben était précédemment Analyste Financier au sein du Customized Fund Investment Group de Crédit Suisse Private Equity où il a réalisé des investissements de croissance dans des entreprises de divers secteurs et analysé des opportunités d’investissements dans des fonds d’investissement. Ben est diplômé de Harvard Business School (MBA) et de Swarthmore College (Economie et Chimie). Il est également détenteur de la certification internationale CFA (Chartered Financial Analyst).
                    </p>
                </td>
            </tr>

            <tr>
                <td width="160">
                    <img src="/Images/Robert.jpg" alt="Robert Leke" width="150" height="154" />
                </td>
                <td valign="top">
                    <h2>Robert Leke
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Robert est Directeur chez Tana Africa Capital. Robert était précédemment Analyste Financier chez Transcentury Limited, holding d’investissement basée au Kenya, où il a participé à l’exécution de transactions et le lancement de nouvelles entreprises en RDC, Afrique du Sud et la Tanzanie. Robert a débuté sa carrière en tant qu’Analyste chez McKinsey dans les bureaux de New York puis de Dubaï. Robert est diplômé de Harvard Business School (MBA) et de Massachusetts Institute of Technology (Ingénierie Electrique et Informatique).
                    </p>
                </td>
            </tr>
            <tr>
                <td width="160">
                    <img src="/Images/Kwabena-Bucknor.jpg" width="150" height="154" alt="Kwabena Bucknor" title="Kwabena Bucknor" /> </td>
                <td valign="top">
                    <h2>Kwabena Bucknor
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Kwabena est Analyste Financier Senior chez Tana Africa Capital. Kwabena était précédemment Chargé de
                        Mission au sein de la division « Corporate Finance » (Fusions &amp; Acquisitions) de Standard Bank
                        Group à Johannesburg. Il a débuté sa carrière en tant qu’Analyse Financier spécialisé́ en gestion
                        de patrimoine à Morgan Stanley Smith Barney à New York. Kwabena est diplômé de Cornell University
                        (Economie et Relations Internationales) et de l’Université de Pékin (Certificat d’études avancées
                        en Mandarin).</p>
                </td>
            </tr>
            <tr>
                <td width="160">
                    <img src="/Images/Nicolas-Hindi.jpg" width="150" height="154" alt="Nicolas Hindi" title="Nicolas Hindi" /> </td>
                <td valign="top">
                    <h2>Nicolas Hindi
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Nicolas est Analyste Financier Senior chez Tana Africa Capital. Nicolas était précédemment Analyste à
                        Deutsche Bank à Londres au sein de la division de Fusions &amp; Acquisitions qui couvre les institutions
                        financières (FIG) en Europe et au Moyen-Orient. Il a élaboré des modèles financiers et participé
                        à des activités de due diligence pour des opérations d’apport secondaire de capital ou d’acquisitions.
                        Nicolas est diplômé de l’Université́ Américaine de Beyrouth (Mathématiques et Finance).</p>
                </td>
            </tr>
            <tr>
                <td width="160">
                    <img src="/Images/Arthur Engotto.png" width="150" height="154" alt="Arthur Engotto" title="Arthur Engotto" /> </td>
                <td valign="top">
                    <h2>Arthur Engotto
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Arthur est Analyste Financier Senior chez Tana Africa Capital. Arthur était précédemment Analyste au sein
                        de Rothschild &amp; Co à Paris, où il est intervenu sur plusieurs opérations de Fusions &amp; Acquisitions
                        et de conseil en financement pour des clients de taille significative dans divers secteurs d’activités.
                        Avant de rejoindre Rothschild &amp; Co, il a également travaillé en Fusions &amp; Acquisitions chez
                        Oddo &amp; Cie. à Paris et successivement dans les départements d’Audit et Transaction Services de
                        KPMG à Paris. Arthur est titulaire d’une Maîtrise en Comptabilité et Finance de l’Université Catholique
                        d’Afrique Centrale et d’un Master en Finance et Stratégie de Sciences Po Paris.
                    </p>
                </td>
            </tr>
            <tr>
                <td width="160">
                    <img src="/Images/Kartik_Das.jpg" width="150" height="154" alt="Kartik Das" title="Kartik Das" /> </td>
                <td valign="top">
                    <h2>Kartik Das
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Kartik est Analyste Financier chez Tana Africa Capital. Kartik était précédemment Analyste Financier
                        chez RLJ Equity Partners à Maryland (Etats Unis) où il a élaboré des modèles financiers et participé
                        à des activités de due diligence pour des opérations d’apport secondaire de capital et d’acquisitions
                        avec levier financier (LBOs) dans divers secteurs d’activités. Avant de joindre RLJ Equity Partners,
                        il a également travaillé en Fusions &amp; Acquisitions chez Deloitte à Los Angeles. Kartik est diplômé
                        de l’Université́ Claremont McKenna (Mathématiques et Economie et Master en Finance).
                    </p>
                </td>
            </tr>
            <tr>
                <td width="160">
                    <img src="/Images/team_daria_katemauswa.JPG" width="150" height="154" alt="Daria Katemauswa" title="Daria Katemauswa" /> </td>
                <td valign="top">
                    <h2>Daria Katemauswa
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Daria est Directeur Administratif et Financier chez Tana Africa Capital. Daria était précédemment Directeur
                        de Mission dans le département Transaction Services de PricewaterhouseCoopers au bureau de Johannesburg
                        où elle a aussi travaillé dans le département Audit en tant que Superviseur. Daria est diplômée de
                        University of South Africa et est expert-comptable certifié en Afrique du Sud et au Zimbabwe.
                    </p>
                </td>
            </tr>
            <tr>
                <td width="160">
                    <img src="/Images/Yolande Kouassi.jpg" width="150" height="154" alt="Yolande Kouassi" title="Yolande Kouassi" /> </td>
                <td valign="top">
                    <h2>Yolande Kouassi
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Yolande est Directeur Administratif et Financier chez Tana Africa Capital à Abidjan. Yolande était précédemment
                        Directeur Administratif et Financier de Agro West Africa à Abidjan après avoir occupé le poste de
                        Responsable Administratif et Financier chez IRES à Abidjan. Elle a débuté sa carrière en tant qu’Auditeur
                        Financier à PricewaterhouseCoopers au bureau d’Abidjan. Yolande est diplômée de l’ESC de Nice.
                    </p>
                </td>
            </tr>
            <tr>
                <td width="160">
                    <img src="/Images/team_rosy_tsotsotso.jpg" width="150" height="154" alt="Rosy Tsotsotso" title="Rosy Tsotsotso" /> </td>
                <td valign="top">
                    <h2>Rosy Tsotsotso
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Rosy est Assistante de Direction chez Tana Africa Capital. Rosy était précédemment Assistante de Direction
                        du Directeur Général de CNBC Africa à Johannesburg après avoir été Assistante de Direction au Ministère
                        de l’Environnement et du Tourisme à Pretoria. Avant cela, Rosy a été Assistante de Direction à Cricket
                        South Africa, la fédération sud-africaine de cricket, à Johannesburg.
                    </p>
                </td>
            </tr>
            <tr>
                <td width="160">
                    <img src="/Images/Lynda Kouawo.jpg" width="150" height="154" alt="Lynda Kouawo" title="Lynda Kouawo" /> </td>
                <td valign="top">
                    <h2>Lynda Kouawo
                        <span style="font-size:16px"></span>
                    </h2>
                    <p align="justify">Lynda est Assistante de Direction chez Tana Africa Capital. Lynda était précédemment Assistante de Direction
                        à Nestlé Côte d’Ivoire après avoir été Assistante de Direction à PricewaterhouseCoopers au bureau
                        d’Abidjan.
                    </p>
                </td>
            </tr>

            <tr>
				<td width="160">
					<img src="/Images/Yannick_Sanni_Picture.jpg" width="150" height="154" alt="Yannick Sanni" title="Yannick Sanni" /> </td>
				<td valign="top">
					<h2>Yannick Sanni 
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Yannick est Analyste Financier chez Tana Africa Capital. Yannick a précédemment travaillé chez HSBC Global Banking au sein des salles de marché des produits à taux fixe successivement à Paris et à Londres. Yannick est diplômé de la London Business School (Master en Finance), de SKEMA Business School (Master en Ingénierie Financière) et de l’Ecole Supérieure de Gestion en France (Licence en Comptabilité et Finance).
					</p>
				</td>
			</tr>

			<tr>
				<td width="160">
					<img src="/Images/Annabel.jpg" width="150" height="154" alt="Annabel Barnicke" title="Annabel Barnicke" /> </td>
				<td valign="top">
					<h2>Annabel Barnicke 
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Annabel est Analyste Financier chez Tana Africa Capital. Annabel était précédemment Analyste Financier dans le fonds d’investissement du Canada Pension Plan Investment Board (CPPIB) à Toronto, où elle a participé à diverses opérations de valorisation et d’acquisition de sociétés dans les secteurs de la technologie et de la distribution. Avant de rejoindre le CPPIB, Annabel a travaillé en Fusions et Acquisitions chez Barclays Investment Bank à New York dans le secteur des produits industriels. Annabel est diplômée de l’Université John Hopkins (Economie et Etudes Internationales).
					</p>
				</td>
			</tr>

        </table>

    </asp:Content>