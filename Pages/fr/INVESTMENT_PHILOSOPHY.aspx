﻿<%@ Page Title="Philosophie d’Investissement | Tana" Language="C#" MasterPageFile="FrenchMasterPage.master" AutoEventWireup="true" CodeFile="FOCUS_SECTORS_AND_INVESTMENT_CRITERIA.aspx.cs" Inherits="Pages_FOCUS_SECTORS_AND_INVESTMENT_CRITERIA" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>
            SetMenu = "About";
        </script>
        <meta name="description" content="All information regarding Tana's approach to investments and the three options they have in this regad" />
        <meta name="keywords" content="Active Investment Management, Long-Term Investment Focus, Clear Sector Focus, strategy and operations across Africa" /> </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <span class="breadCrumb">You are Here : <a href="../Default.aspx">Accueil</a> > À Propos > Philosophie d’Investissement</span>
        <br />
        <br />
        <h1><strong>PHILOSOPHIE D’INVESTISSEMENT</strong></h1>
        <p align="justify">La philosophie d’investissement de Tana repose sur les piliers suivants:</p>
        <h2>Gestion Active Des Investissements</h2>
        <p align="justify">Tana est un investisseur activement impliqué dans la gestion des entreprises dans lesquelles elle investit en favorisant une approche positive et collaborative avec les autres actionnaires et les équipes dirigeantes des entreprises en question. Tana a pour priorités l'institutionnalisation de pratiques de gestion saines telles que le leadership fondé sur la compétence, la discipline financière, l'excellence opérationnelle et la bonne gouvernance. Pour atteindre ses objectifs, Tana est en mesure de s’appuyer sur l’expérience acquise par son équipe dirigeante en Afrique, aussi bien du point de vue opérationnel que stratégique.
        </p>
        <h2>Horizon d’Investissement à Long Terme</h2>
        <p align="justify">L’approche d’investissement collaborative et active que Tana privilégie requiert que Tana limite le nombre d’entreprises dans lesquelles elle investit et concentre ses efforts sur les entreprises qui peuvent servir de plateforme pour une expansion nationale, régionale voire continentale. Le développement d’entreprises pérennes nécessite parfois une implication de Tana dans la durée avec un horizon d’investissement plus long que la norme. La flexibilité́ dont Tana dispose par rapport à son horizon d’investissement provient du fait que Tana a été constituée en tant que holding d'investissement à capital permanent et qu’elle a pour actionnaires des investisseurs qui doivent leur succès au fait qu’ils rejettent le court-termisme très prévalent actuellement dans une grande partie du monde de la finance.
        </p>
        <h2>Positionnement Sectoriel Clair</h2>
        <p align="justify">Tana se focalise en priorité́ sur les secteurs liés aux biens de grande consommation, l’éducation, le commerce de détail, la santé et l’agro-industrie. Il en résulte une expertise sectorielle profonde et ciblée, gage de forte crédibilité́ et permettant de capitaliser sur la forte expérience de terrain accumulée en Afrique par l’équipe dirigeante de Tana.
        </p>
		<h2>Fondamentaux Sains et Volatilité à Court Terme Assumée</h2>
        <p align="justify">Tana se concentre sur les fondamentaux et les perspectives de développement à long terme des entreprises dans lesquelles elle investit plutôt que de se préoccuper de la volatilité́, parfois observée, à court terme de leur performance financière.
        </p>

    </asp:Content>