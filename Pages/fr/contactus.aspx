﻿<%@ Page Language="C#" MasterPageFile="FrenchMasterPage.master" AutoEventWireup="true"
    CodeFile="contactus.aspx.cs" Inherits="pages_contactus" Title="Contact Us | Tana" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <script>
            SetMenu = "contact";
        </script>
        <meta name="description" content="Tana's contact information should you wish to contact us with any questions or queries" />
        <meta name="keywords" content="Office Address, Phone number, email address, physical address, investment manager" />
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <span class="breadCrumb">You are Here : <a href="../Default.aspx">Accueil</a> > Contact</span>
        <br />
        <br />
        <h1><strong>NOUS CONTACTER</strong></h1>
        <table width="100%" cellpadding="0" cellspacing="0" align="left">
            <tr>
                <td valign="top">
                    <br />
                    <h2><strong>Tana Africa Capital Managers (Pty) Ltd</strong></h2>
                    <p> 6 St Andrews Road, Parktown 2193<br /><br />Johannesburg, Afrique du Sud</p>

                    <p><strong>Phone:</strong> <a href="tel:+27112742130">+27 11 274 2130</a><br /><br />
                        <strong>Fax:</strong> <a href="tel:+27112742131">+27 11 274 2131</a><br />
                    </p>
                </td>
				</tr>
				<tr>
				                <td valign="top">
                    <br />
                    <h2><strong>Tana Africa Investment Managers Côte d’Ivoire</strong></h2>
                    <p>Rond-point Mel Théodore – Boulevard Arsène Usher Assouan<br /><br />Riviéra, Abidjan<br/><br/>25 BP 174 Abidjan 25, Côte d’Ivoire</p>

                    <p><strong>Phone:</strong> <a href="tel:+22522477510">+225 22 47 75 10</a><br /><br />
                        <strong>Fax:</strong> <a href="tel:+22522477519">+225 22 47 75 19</a><br />
                    </p>
                </td>
                <td width="410" height="330" valign="top" style="background-image:url(../Images/form-bg.jpg); padding-top: 10px; background-repeat:no-repeat;display:none">
                    <table width="96%" cellpadding="0" cellspacing="7" align="center">
                        <tr>
                            <td width="80" class="Form"> Full Name </td>
                            <td width="200">
                                <asp:TextBox ID="txtFullName" runat="server" Width="250" CssClass="TextBox"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form"> Company </td>
                            <td>
                                <asp:TextBox ID="txtCompany" runat="server" Width="250" CssClass="TextBox"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form"> Email Address </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" Width="250" CssClass="TextBox"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form"> Comments </td>
                            <td>
                                <asp:TextBox ID="txtComments" runat="server" Width="250" CssClass="TextBox" TextMode="MultiLine" Height="120"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnSend" runat="server" CssClass="Button" Text="Send" OnClick="btnSend_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td id="tdScr" runat="server"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table cellpadding="0" cellspacing="0" width="100%">
            <tr valign="top">
                <td style="" valign="top">&nbsp;</td>
                <td>
                    <table width="100%" class="Form">
                        <tr>
                            <td width="938" align="left">&nbsp;</td>
                        </tr>
                    </table>

                    <br />
                    <p>&nbsp;</p>
                </td>
            </tr>
        </table>
    </asp:Content>