﻿<%@ Page Title="Welcome to Tana" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Pages_Default" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>        SetMenu = "Default";</script>
        <meta name="description" content="Tana Africa Capital is an Africa-focused investment company founded by E. Oppenheimer & Son International Ltd and Temasek."
        />
        <meta name="keywords" content="Africa-focused investment, African knowledge, Ethiopia’s Lake Tana, capital" />
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <table cellpadding=0 cellspacing=0 border="0">
            <tr valign=top>
                <td>
                    <h1>
                        <strong>Welcome</strong>
                    </h1>
                    <p align="justify">Tana Africa Capital is an evergreen Africa-focused investment company founded by E. Oppenheimer &amp;
                        Son and Temasek. Through the medium of capital and business-building support, Tana aims to build
                        African companies for generations to come. In doing so, Tana is able to draw on the rich heritage,
                        vast experience and extensive networks of its founding shareholders, as well as the on-the- ground
                        African knowledge and operating experience of its management and advisory team. Like its namesake,
                        Ethiopia’s Lake Tana (the source of the Blue Nile), Tana aims to serve as a fount of development,
                        leaving an enduring, tangible and positive legacy on the businesses which it supports.</p>
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
    </asp:Content>