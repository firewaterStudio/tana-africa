﻿using System;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;

public partial class pages_contactus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (txtFullName.Text == "")
            tdScr.InnerHtml = "<script>alert('Error: Please Check your details!');</script>";
        else if (txtCompany.Text == "")
            tdScr.InnerHtml = "<script>alert('Error: Please Check your details!');</script>";
        else if (txtEmail.Text == "")
            tdScr.InnerHtml = "<script>alert('Error: Please Check your details!');</script>";
        else if (txtComments.Text == "")
            tdScr.InnerHtml = "<script>alert('Error: Please Check your details!');</script>";
        else
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<html>");
                sb.Append("<body>");
                sb.Append("Hello,");
                sb.Append("<br />");
                sb.Append("Email from Tana website");
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("Name: " + txtFullName.Text);
                sb.Append("<br />");
                sb.Append("Email: " + txtEmail.Text);
                sb.Append("<br />");
                sb.Append("Comapny: " + txtCompany.Text);
                sb.Append("<br />");
                sb.Append("Comments: " + txtComments.Text.Replace("\n", "<br />"));
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("Regards,");
                sb.Append("<br />");
                sb.Append("Tana Website");
                sb.Append("<br />");
                sb.Append("</body>");
                sb.Append("</html>");

                #region Custom Headers
                string strHeaders = "mailFrom|BARC Website";
                string[] strCustomHeaders = strHeaders.Split('§');
                #endregion

                #region Attachments
                string[] strAttach = "".Split('§');
                #endregion

                bool Success = DataAccessor.SendMailHTML(sb.ToString(), "Contact Us From Tana",
                    ConfigurationManager.AppSettings["SMTPTo"], ConfigurationManager.AppSettings["SMTPTo"],
                    "","",
                    txtEmail.Text, txtEmail.Text,
                    strCustomHeaders, strAttach);

                if (Success)
                    tdScr.InnerHtml = "<script>alert('Thank you for sending us your feedback, \\nwe will respond as soon as possible.');" +
                        "document.location.href='default.aspx'</script>";
                else
                    tdScr.InnerHtml = "<script>alert('Error: Please Check your details!');</script>";
            }
            catch (Exception x) { tdScr.InnerHtml = "<script>alert('Error: " + x.Message + "');</script>"; }
        }
    }
}
