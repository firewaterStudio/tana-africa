﻿<%@ Page Title="Founding Shareholders | Tana" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true" CodeFile="Team.aspx.cs" Inherits="Pages_Team" %>

  <asp:Content ID="Content3" ContentPlaceHolderID="head" Runat="Server">
    <script>        SetMenu = "About";</script>
    <meta name="description" content="History of the founding shareholders who began and started Tana Africa" />
    <meta name="keywords" content="E. Oppenheimer & Son, Anglo American, De Beers, Temasek, inancial services; transportation, logistics and industrials, telecommunications, media & technology, life sciences, energy and resources."
    />
  </asp:Content>
  <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <span class="breadCrumb">You are Here :
      <a href="../Default.aspx">Home</a> > About > Founding Shareholders</span>
    <br />
    <br />
    <h1>
      <strong>Founding</strong> Shareholders</h1>
    <p align="justify">Tana Africa Capital was established in 2011 as a 50/50 joint venture between E. Oppenheimer & Son and Temasek. The two
      like-minded investors were united by the common vision of creating value on the African continent through capital and
      business building support. To date the company has deployed more than US$250 million across the length and breadth
      of Africa.</p>

    <h2>E. Oppenheimer &amp; Son</h2>
    <p align="justify">E. Oppenheimer &amp; Son holds the Oppenheimer Family interests, founders of the global mining company Anglo American
      (
      <a href="http://www.angloamerican.com" target="_blank">www.angloamerican.com</a>), and until August 2012, managing shareholders of De Beers (
      <a href="http://www.debeersgroup.com" target="_blank">www.debeersgroup.com</a>), the world’s leading diamond company. E. Oppenheimer & Son has an extensive private equity
      portfolio investing in leading private equity funds in the United States, Europe and Asia. It also invests in South
      African companies, often working alongside leading South African corporations and private equity firms. The Oppenheimer
      Family interests have also established a public equity investment vehicle and have made an allocation for infrastructure-related
      investments across the continent.
      <br />
      <br /> In addition to its commercial holdings, The Oppenheimer Family established the Brenthurst Foundation (
      <a href="http://www.thebrenthurstfoundation.org" target="_blank">www.thebrenthurstfoundation.org</a>) as both a forum for public debate and policy-advisory service to African and other
      governments. The Brenthurst Foundation focuses on identifying and applying best practice from high-growth economies,
      and has served on request as an advisor to government in a number of African countries, including Rwanda, Liberia,
      Mozambique, Lesotho, Kenya, Swaziland and Zimbabwe, and also in Afghanistan. The Oppenheimer Family interests have
      deep African roots, with four generations of investing experience on the continent. They have an intimate understanding
      of doing business in Africa, and a commitment to furthering the socio-economic development of the continent. </p>
    <!-- <p><strong>E. Oppenheimer &amp; Son’s notable investments include:</strong></p>
  
  <table width="94%" border="0" align="center" cellpadding="7" cellspacing="1" >
    <tr bgcolor="white">
      <td valign="top" ><h3>DeBeers Group</h3>        
      The DeBeers Group is the world’s leading diamond company, employing ~20,000 people on 5 continents with 85% of the workforce employed in Africa. EOS currently owns 40% of DeBeers, with Anglo American owning a further 45% of the company and the government of Botswana holding the remaining 15%</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top" ><h3>Anglo American</h3>        
      Anglo American Plc is one of the world’s largest mining and natural resource groups, with sales of $25b in 2009 and a total market capitalization of ~$36b. EOS currently owns 2.26% of the outstanding shares of Anglo American Plc, and Nicky Oppenheimer sits on the company’s board as a Non-Executive Director.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top" ><h3>Stockdale Street - London</h3>        
      SSC London invests in leading private equity, hedge funds and endowment funds in the United States, Europe and Asia.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top" ><h3>Stockdale Street - South Africa</h3>SSC South Africa invests directly in South African companies, often working alongside leading South African corporations and private equity firms. </td>
    </tr>
    <tr bgcolor="white">
      <td valign="top" ><h3>Brenthurst - Foundation</h3>
      The Oppenheimer Family founded the Brenthurst Foundation in April 2005 as a think-tank to contribute to the debate around strategies and policies for strengthening Africa’s economic performance. The Brenthurst Foundation has served as a trusted advisor to senior government officials in a number of African countries, including Rwanda, Liberia, Mozambique, Lesotho, Kenya, Swaziland and Zimbabwe.</td>
    </tr>
  </table>
  <br /> -->
    <h2>Temasek </h2>
    <p align="justify">
      Incorporated in 1974, Temasek is an investment company headquartered in Singapore (<a href="http://www.temasek.com.sg" target="_blank">www.temasek.com.sg</a>). Supported by 11 offices
      globally, Temasek owns a diversified S$308 billion (US$235 billion) portfolio as at 31 March 2018, concentrated principally
      in Singapore and Asia. </p>
    <!--<strong>Temasek’s notable investments include:</strong><br />
  </p>
  <table width="94%" border="0" align="center" cellpadding="7" cellspacing="1">
    <tr bgcolor="white">
      <td valign="top"><h3>Financial Services</h3>        
      Bank of China Limited, China Construction Bank Corporation, DBS Group Holdings, PT Bank Danamon Indonesia, Standard Chartered PLC, ICICI Bank Limited.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top"><h3>Telecommunications, Media &amp; Technology</h3>        Shin Corporation Public Company, Singapore Technologies Telemedia, STATS ChipPAC, Bharti Airtel, MediaCorp Pte. Ltd, Singapore Telecommunications Limited.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top"><h3>Transportation &amp; Industrials</h3>        Keppel Corporation, Neptune Orient Lines, PSA International Pte Ltd, Sembcorp Industries Ltd, Singapore Technologies, Singapore Airlines, Singapore Power, SMRT Corporation Ltd.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top"><h3>Life Sciences, Consumer &amp; Real Estate</h3>        Olam International Limited, CapitaLand Limited, Li &amp; Fung Limited, Mapletree Investments, SATS Ltd, Surbana Corporation, Wildlife Reserves.</td>
    </tr>
    <tr bgcolor="white">
      <td valign="top"><h3>Energy &amp; Resources</h3>        Chesapeake Energy, MEG Energy.</td>
    </tr>
  </table> -->
    <p align="justify">Temasek's investment themes centre on Transforming Economies, Growing Middle Income Populations, Deepening Comparative
      Advantages and Emerging Champions. Its portfolio covers a broad spectrum of industries including financial services,
      transportation, logistics and industrials, telecommunications, media and technology, life sciences, consumer and real
      estate, energy and resources.
    </p>
    <p align="justify">For more information, please visit
      <a href="http://www.temasek.com.sg" target="_blank">www.temasek.com.sg</a>
    </p>
  </asp:Content>