﻿<%@ Page Title="Investment Philosophy | Tana" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true" CodeFile="FOCUS_SECTORS_AND_INVESTMENT_CRITERIA.aspx.cs" Inherits="Pages_FOCUS_SECTORS_AND_INVESTMENT_CRITERIA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>        SetMenu = "About";</script>
    <meta name="description" content="All information regarding Tana's approach to investments and the three options they have in this regad" />
    <meta name="keywords" content="Active Investment Management, Long-Term Investment Focus, Clear Sector Focus, strategy and operations across Africa" />    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<span class="breadCrumb">You are Here : <a href="../Default.aspx">Home</a> > About > Investment Approach</span>
<br />
<br />
<h1><strong>Investment Approach</strong></h1>
<p align="justify">Tana’s investment approach is predicated on the following core principles:</p>
<h2>Active Investment Management</h2>
<p align="justify">Tana is an active, value orientated investor that engages positively and in a collaborative fashion with the boards and management of the companies in which it invests. Tana prioritises the institutionalisation of business practices such as business leadership, financial discipline, operational excellence and sound corporate governance. In doing so, it is able to draw on its investment team’s deep experience in strategy and operations across Africa.
</p>
<h2>Long-Term Investment Focus</h2>
<p align="justify">The value-adding, partnership approach is supported by the fact that Tana limits the number of companies in which it invests, focusing its efforts on companies that can serve as platforms for domestic and regional expansion. Developing such enduring businesses will, at times, require the adoption of lengthy investment time horizons. The flexibility that Tana has in terms of investment holding periods originates from the fact that it has been established as an evergreen investment company and has as its founding shareholders, investors that have achieved great success from eschewing the short termism that dominates much of the investment world.
  </p>
<h2>Clear Sector Focus</h2>
<p align="justify">Tana focuses primarily on FMCG, retail, education, healthcare and agribusiness. This clear sector focus brings with it deep industry expertise and extensive networks, greatly enhancing the positive impact that Tana can have on the companies in which it invests.
</p>
<h2>Underlying Health, Not Short Term Volatility</h2>
<p align="justify">Tana focuses on the underlying health and long term prospects of the businesses in which it invests, rather than be preoccupied by short term swings in financial performance.
</p>

</asp:Content>
