﻿<%@ Page Title="Realised Exits | Tana" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true" CodeFile="Investments.aspx.cs" Inherits="Pages_Investments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>         SetMenu = "Investments";</script>
    <meta name="description" content=" Investments with which Tana Africa works with" />
    <meta name="keywords" content="Promisador Nigeria, Promasidor Group, affordable, small sachets, powdered milk, food seasoning and tea brands" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <span class="breadCrumb">You are Here : <a href="../Default.aspx">Home</a> > Realised Exits</span>
<br />
<br />
<h1 class="investExit"><strong>REALISED EXITS</strong></h1><br/><br/>
 <h2>Minority Interest in Promasidor Group (Promasidor)</h2>
 <p align="justify"><strong>Promasidor  at a glance: </strong>Promasidor Group (<a href="http://www.promasidor.com" target="_blank">www.promasidor.com</a>) is a privately held fast moving consumer goods company with industrial operations in nine African countries and sales into thirty countries. Founded in the Democratic Republic of Congo in 1979 with the revolutionary offering of milk powder sold in affordable, small sachets, the Group now carries a wide range of powdered milk, food seasoning and tea brands. Promasidor enjoys strong market positions in Anglophone and Francophone West Africa, as well as a growing presence in East and Southern Africa.</p>

 <p align="center"><img src="/Images/promasidor.jpg" alt="promasidor" title="promasidor" width="400"  /></p>
 <br />
<br />
 <h2>Minority Interest in Société d’Investissement pour l’Agriculture Tropicale (S.I.A.T)</h2>
 <p align="justify"><strong>SIAT at a glance: </strong>SIAT (<a href="http://www.siat-group.com" target="_blank">www.siat-group.com</a>) owns ~70,000 ha of natural rubber and palm oil plantations in West Africa in the following countries: Gabon:  natural rubber, palm oil and a cattle ranch; Côte d’Ivoire: natural rubber; Nigeria: palm oil; Ghana: palm oil. SIAT owns a palm oil mill and a refinery at all its palm oil plantations. SIAT is also involved in cattle ranching and the production and distribution of cooking oil and soap production in Gabon. SIAT head office is located in Brussels, Belgium</p>

 <p align="center"><img src="/Images/siat-1.jpg" alt="siat-1.jpg" title="siat-1.jpg" width="200"  />
		<img src="/Images/siat-2.jpg" alt="siat-2" title="siat-2" width="200"  />
		<img src="/Images/siat-3.jpg" alt="siat-3.jpg" title="siat-3.jpg" width="200"  /></p><br /><br/>
 

 </asp:Content>
