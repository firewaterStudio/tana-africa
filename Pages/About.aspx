﻿<%@ Page Title="Company Overview | Tana" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="Pages_About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>         SetMenu = "About";</script>
    <meta name="description" content="All information pertainng to Tana Africa and the history behind the company" />
    <meta name="keywords" content="E. Oppenheimer & Son, Temasek, Anglo American, De Beers, Brenthurst Foundation" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<span class="breadCrumb">You are Here : <a href="../Default.aspx">Home</a> > About Us > Company Overview</span>
<br />
<br />
  <h1><strong>Company Overview</strong></h1> xc 

  <p align="justify">Tana Africa Capital is an Africa-focused investment company founded by E. Oppenheimer &amp; Son and Temasek Holdings.  With US$300m of assets under management, Tana Africa invests between US$20m and US$75m to acquire significant minority or control equity positions in established businesses in the Consumer and Agriculture sectors. </p>
  <p align="justify"><strong>Within the Consumer sector, Tana targets the following areas: </strong></p>

              <ul>
                <li>Food, beverage and personal care fast moving consumer goods</li>
                <li>Building materials</li>
                <li>Retail</li>
                <li>Logistics</li>
              </ul>
  <p align="justify">We also consider investments in Consumer financial services, Media, Healthcare and Education</p>	
  <p align="justify"><strong>In Agriculture, Tana invests in:</strong></p>

              <ul>
                <li>Input providers (fertiliser, seeds, crop protection and equipment)</li>
                <li>Value-added services (logistics, insurance and finance) </li>
                <li>Processing</li>
              </ul>

  <p align="justify">Tana aims to build African business institutions for generations to come.  The company is competitively positioned to draw on the rich heritage, vast experience and extensive networks of its founding shareholders, as well as on the on-the-ground African knowledge and operating experience of its management team. Like its namesake, Ethiopia’s Lake Tana (the source of the Blue Nile), Tana Africa aims to serve as a fount of development, leaving an enduring, tangible and positive legacy on the businesses which it supports.
              <br />
              <br />



  <p>&nbsp;</p>
</asp:Content>
