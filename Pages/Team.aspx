﻿<%@ Page Title="Management Team | Tana" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true" CodeFile="Team.aspx.cs" Inherits="Pages_Team" %>

	<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
		<script>         SetMenu = "Team";</script>
		<meta name="description" content="List of staff who form the Tana team" />
		<meta name="description" content="Managing Director, Principal, Senior Associate, Finance Manager, Executive Assistant" />
	</asp:Content>
	<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
		<span class="breadCrumb">You are Here :
			<a href="../Default.aspx">Home</a> > Team > Management Team</span>
		<br />
		<br />
		<h1>
			<strong>Team</strong></h1>
		<p align="justify">Tana is managed and advised by a team with many years of Africa-specific experience in private equity,
			management consulting, mergers and acquisitions, merger integration and transaction services.

		</p>
		<table width="100%" cellpadding="10" cellspacing="0" border="0">
			<tr>
				<td width="160">
					<img src="/Images/team_duncan_randall.JPG" width="150" height="154" alt="Duncan Randall" title="Duncan Randall" /> </td>
				<td>
					<h2>Duncan Randall
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">

						Duncan co-founded Tana Africa Capital and is a member of its Investment Committee. Previously, he was a Managing Director
						of Africa Holdings, an investment fund of E. Oppenheimer &amp; Son. Prior to this, Duncan established a private equity
						firm in South Africa, then designed, raised finance for, and launched two successful funds specialising in growth and
						buyout financing. He was also a McKinsey &amp; Company Consultant in the Johannesburg office, with extensive work in
						Africa across fast moving consumer goods and other sectors. Duncan holds a Bachelor's degree in African Politics from
						the University of Witwatersrand, and a DPhil Politics from Oxford University, where he was a Rhodes Scholar.
					</p>
				</td>
			</tr>

			<!--<tr>
	  <td width="160">
      	<img src="/Images/team_fabrice_ndjodo.JPG" width="150" height="154" alt="Fabrice Ndjodo" title="Fabrice Ndjodo" />      </td>
      <td>         
		 <h2>Fabrice Ndjodo <span style="font-size:16px">– Managing Director, Abidjan</span></h2>
		 <p align="justify">Fabrice is a Managing Director at Tana Africa Capital.  Previously, he was an Executive Director of Liquid Africa, a financial advisory firm focused on raising capital in Sub Saharan Africa. He also 

worked as a sell-side Research Analyst for Macquarie First South Securities, where he was ranked the second top Diversified Industrial Analyst in South Africa. Prior to this, he was an Associate in the Corporate Finance Division of 

Macquarie USA in New York, and served as an Investment Analyst for IFC in Washington, DC, focusing on projects in Europe, South America and Africa.  Fabrice holds a MSc in Finance and International Management from HEC Paris, and an MBA 

from Harvard Business School.</p>	 
      </td>
  </tr>-->
			<tr>
				<td width="160">
					<img src="/Images/Pieter.jpg" width="150" height="154" alt="Pieter Cilliers" title="Pieter Cilliers" /> </td>
				<td>
					<h2>Pieter Cilliers
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Pieter is a Principal at Tana Africa Capital. Prior to Tana, he was a Principal at Bain & Company
						in Brazil, where he led projects in the private equity, FMCG, education and healthcare sectors. He also worked as a
						Vice President at Hancock Park Associates, a Los Angeles based private equity fund focused on US mid-cap buyouts. Previously
						he was a Manager at Deloitte’s M&amp;A Group in San Francisco. Pieter holds a Bachelor’s degree in Accounting from
						the University of Stellenbosch, an MBA from INSEAD, a Chartered Financial Analyst® designation from the CFA Institute,
						and is a certified Chartered Accountant in South Africa.</p>
				</td>
			</tr>
			<tr>
				<td width="160">
					<img src="/Images/team_ben_munda.jpg" width="150" height="154" alt="Ben Munda" title="Ben Munda" /> </td>
				<td>
					<h2>Ben Munda
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">

						Ben is a Principal at Tana Africa Capital. Prior to Tana, he was an Investment Professional in the Customized Fund Investment Group of Credit Suisse Private Equity, where he made growth equity investments across several sectors, and analysed investments into private equity funds. Ben holds a Bachelor's degree in Economics and Chemistry from Swarthmore College (USA), an MBA from Harvard Business School, and a Chartered Financial Analyst® designation from the CFA Institute.
					</p>
				</td>
			</tr>

			<tr>
				<td width="160">
					<img src="../Images/Robert.jpg" alt="Robert Leke" width="150" height="154" />
				</td>
				<td valign="top">
					<h2>Robert Leke
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Robert is a Principal at Tana Africa Capital. Previously Robert was an Investment Professional in the South Africa office of TransCentury Limited, a Kenya-based investment firm, where he supported deal execution as well as the launch of new businesses in the DRC, South Africa and Tanzania. He began his career as a Business Analyst at McKinsey & Co. in the New York and Dubai offices. Robert holds a Bachelor of Science degree in Electrical Engineering and Computer Science from the Massachusetts Institute of Technology (MIT) and an MBA from Harvard Business School.
						</p>
				</td>
			</tr>
			<tr>
				<td width="160">
					<img src="../Images/Kwabena-Bucknor.jpg" width="150" height="154" alt="Kwabena Bucknor" title="Kwabena Bucknor" /> </td>
				<td valign="top">
					<h2>Kwabena Bucknor
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Kwabena is a Senior Associate at Tana Africa Capital. Previously, he was an Associate in the Corporate
						Finance M&amp;A division at Standard Bank in Johannesburg. Prior to this, he worked as an Analyst in Global Wealth
						Management at Morgan Stanley in New York. Kwabena holds a Bachelor's degree in Economics and International Relations
						from Cornell University (USA).
					</p>
				</td>
			</tr>
			<tr>
				<td width="160">
					<img src="../Images/Nicolas-Hindi.jpg" width="150" height="154" alt="Nicolas Hindi" title="Nicolas Hindi" /> </td>
				<td valign="top">
					<h2>Nicolas Hindi
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Nicolas is a Senior Associate at Tana Africa Capital. Previously, he was an analyst in the M&amp;A
						Financial Institutions Group at Deutsche Bank in London, where he was involved in major mergers, acquisitions and rights
						issues of banks, insurers and asset managers in Europe, the Middle East and Africa. Nicolas holds a Bachelor's degree
						in Mathematics and Finance from the American University of Beirut (Lebanon).</p>
				</td>
			</tr>

			<tr>
				<td width="160">
					<img src="../Images/Arthur Engotto.png" width="150" height="154" alt="Arthur Engotto" title="Arthur Engotto" /> </td>
				<td valign="top">
					<h2>Arthur Engotto
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Arthur is a Senior Associate at Tana Africa Capital. Previously, he worked in the Investment Banking
						division of Rothschild &amp; Co in Paris, where he was involved in several transactions spanning M&amp;A, Debt and
						Equity advisory for major clients (large capitalisation companies and private equity funds) across various industries.
						Prior to this, he worked in the Audit and Transaction Services departments of KPMG in Paris and in the Investment Banking
						division of Oddo &amp; Cie. Arthur holds a Maîtrise in Accounting from Catholic University of Central Africa and a
						Master&#39;s degree in Finance and Strategy from Sciences Po Paris.</p>
				</td>
			</tr>
			<tr>
				<td width="160">
					<img src="../Images/Kartik_Das.jpg" width="150" height="154" alt="Kartik Das" title="Kartik Das" /> </td>
				<td valign="top">
					<h2>Kartik Das
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Kartik is an Associate at Tana Africa Capital. Previously, he was an Associate at RLJ Equity Partners
						in Maryland, where he was involved in analysing and executing leveraged buyouts focused on profitable middle-market
						companies across several industries. Prior to this, he worked as a Mergers &amp; Acquisitions Analyst at Deloitte Corporate
						Finance in Los Angeles, California. Kartik holds a Bachelor’s degree in Applied Mathematics and Economics and a Master’s
						degree in Finance from Claremont McKenna College (USA).</p>
				</td>
			</tr>


			<tr>
				<td width="160">
					<img src="/Images/team_daria_katemauswa.JPG" width="150" height="154" alt="Daria Katemauswa" title="Daria Katemauswa" />					</td>
				<td valign="top">
					<h2>Daria Katemauswa
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Daria is the Chief Financial Officer of Tana Africa Capital. Previously, she was a Senior Manager
						of Transaction Services at PricewaterhouseCoopers, where she also served as a Senior Associate for Audit. Daria holds
						a BCompt (Hons) from the University of South Africa and is a certified Chartered Accountant in South Africa and Zimbabwe.
					</p>
				</td>
			</tr>

			<tr>
				<td width="160">
					<img src="/Images/Yolande Kouassi.jpg" width="150" height="154" alt="Yolande Kouassi" title="Yolande Kouassi" /> </td>
				<td valign="top">
					<h2>Yolande Kouassi
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Yolande is the Chief Financial Officer of Tana Africa Capital in Abidjan. Previously, she was the
						Chief Financial Officer of Agro West Africa in Abidjan. Prior to that she worked as the Chief Financial Officer of
						IRES in Abidjan. She began her career as Financial Auditor at PricewaterhouseCoopers in Abidjan. Yolande is graduated
						from ESC Nice.
					</p>
				</td>
			</tr>

			<tr>
				<td width="160">
					<img src="../Images/team_rosy_tsotsotso.jpg" width="150" height="154" alt="Rosy Tsotsotso" title="Rosy Tsotsotso" />					</td>
				<td valign="top">
					<h2>Rosy Tsotsotso
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Rosy Tsotsotso in an Executive Assistant at Tana Africa Capital. Previously, she worked in the office
						of the Managing Director for CNBC Africa, the global television broadcaster. Prior to that, Rosy worked in administration
						for Cricket South Africa and the Department of Environmental Affairs and Tourism.
					</p>
				</td>
			</tr>



			<tr>
				<td width="160">
					<img src="../Images/Lynda Kouawo.jpg" width="150" height="154" alt="Lynda Kouawo" title="Lynda Kouawo" /> </td>
				<td valign="top">
					<h2>Lynda Kouawo
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Lynda is an Executive Assistant at Tana Africa Capital. Previously, she worked at Nestlé Côte d’Ivoire.
						Prior to that, Linda worked as Executive Assistant at PricewaterhouseCoopers in Abidjan.
					</p>
				</td>
			</tr>
			<tr>
				<td width="160">
					<img src="../Images/Yannick_Sanni_Picture.jpg" width="150" height="154" alt="Yannick Sanni" title="Yannick Sanni" /> </td>
				<td valign="top">
					<h2>Yannick Sanni 
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Yannick is an Associate at Tana Africa Capital. Previously Yannick worked in Fixed Income Trading at HSBC Global Banking and Markets in the Paris and London offices. Yannick holds a Bachelor’s degree in Accounting & Finance from Ecole Superieure de Gestion (France), a Master’s degree in Financial Engineering from SKEMA Business School (France) and a Master’s degree in Finance from London Business School.
					</p>
				</td>
			</tr>

			<tr>
				<td width="160">
					<img src="../Images/Annabel.jpg" width="150" height="154" alt="Annabel Barnicke" title="Annabel Barnicke" /> </td>
				<td valign="top">
					<h2>Annabel Barnicke 
						<span style="font-size:16px"></span>
					</h2>
					<p align="justify">Annabel is an Associate at Tana Africa Capital. Previously, she was an Associate in the Private Equity Group at Canada Pension Plan Investment Board in Toronto, where she was involved in the evaluation and execution of investment opportunities focused on companies across the Technology and Consumer Retail space. Prior to this, she worked as an Investment Banking Analyst at Barclays Investment Bank in New York, with a focus on Industrials companies. Annabel holds a Bachelor’s degree in International Studies and Economics from Johns Hopkins University (USA).
					</p>
				</td>
			</tr>

		</table>

	</asp:Content>