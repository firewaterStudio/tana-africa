﻿<%@ Page Title="Investments | Tana" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true" CodeFile="Investments.aspx.cs" Inherits="Pages_Investments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>         SetMenu = "Investments";</script>
    <meta name="description" content=" Investments with which Tana Africa works with" />
    <meta name="keywords" content="Promisador Nigeria, Promasidor Group, affordable, small sachets, powdered milk, food seasoning and tea brands" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <span class="breadCrumb">You are Here : <a href="../Default.aspx">Home</a> > Investments</span>
<br />
<br />
 <h1><strong>CURRENT PORTFOLIO</strong></h1><br/>
 <br /> 
 
 <h2>Minority Interest in KITEA S.A. (KITEA)</h2>
 <p align="justify"><strong>KITEA at a glance: </strong>KITEA (<a href="http://www.kitea.ma" target="_blank">http://www.kitea.ma</a>) is the leading Moroccan retailer of household furniture, office furniture and home accessories present across 17 cities and over 20 stores in Morocco, as well as 2 franchise stores in sub-Saharan Africa (Equatorial Guinea and the Democratic Republic of Congo). The company was founded in 1993 with the opening of its first 300 sqm store in Route d'El Jadida in Casablanca with 5 employees. Today KITEA has a cumulated showroom area of over 50,000 sqm under its own brand and the CASA franchise and employs more than 1,000 people. KITEA achieved its initial expansions in Morocco through co-location partnerships with major retailers, and since 2008 has evolved into developing its own big-box shopping centers termed "KITEA Géants”.</p>
<p align="center">
    <img src="/Images/kitea-3.png" alt="kitea-3" title="kitea-3" width="800" /></p>
 <br />
<br />

<h2>Minority Interest in Regina for Pasta and Food Industries S.A.E. (Regina)</h2>
 <p align="justify"><strong>Regina at a glance</strong>: Established in 1986 Regina (<a href="http://www.pastaregina.com" target="_blank">www.pastaregina.com</a>) is Egypt’s second largest pasta manufacturer and owns the country’s only durum wheat flour mill. It enjoys a dominant position in the semolina (durum) pasta market with its Regina and Masreya brands, and also has in its portfolio the fast-growing Star and King soft wheat pasta brands. In addition to a strong local market presence, Regina has an expanding export business targeted at sub-Saharan Africa and the Middle East.</p>

 <p align="center"><img src="/Images/regina.jpg" alt="regina" title="regina" width="438" /></p><br />
<br />

 <h2>Minority Interest in Société Alimentaire de la Nomba (SAN)</h2>
 <p align="justify"><strong>SAN at a glance: </strong>Based in Gabon, SAN (<a href="http://www.san-gel.com" target="_blank">www.san-gel.com</a>) is a privately-held importer, processor, distributor and retailer of frozen and cold food products. The company was established in 1979 and has built a strong offering in frozen poultry, pork, beef, lamb, veal, fish, vegetables, pre-cooked meals, ice creams and desserts. SAN distributes its products through the wholesale channel and through its own retail outlets (branded freezer centres) across Libreville, with strong growth prospects in Gabon and beyond.</p>
<p align="center"><img src="/Images/nomba-1.jpg" alt="nomba-1" title="nomba-1" width="310" />
		<img src="/Images/nomba-2.jpg" alt="nomba-2" title="nomba-2" width="250" />
		<img src="/Images/nomba-3.jpg" alt="nomba-3" title="nomba-3" width="250" /></p>
 <br />
<br />

 <h2>Minority Interest in SANA Education (SANA)</h2>
 <p align="justify"><strong>SANA at a glance</strong>: SANA (<a href="http://www.sanaeducation.com" target="_blank">www.sanaeducation.com</a>) is a K-12 school group that aims to be the leading provider of quality education across Africa. SANA has a distinct education philosophy that strives to attain high international standards without compromising on local cultures and heritage. SANA’s goal is to produce successful, emotionally fulfilled and locally rooted individuals who will pioneer a new era of African excellence in the 21st century. SANA was conceived in July 2015 as a joint-venture between Saham Group and Tana Africa Capital, and currently operates a number of schools in Morocco.</p>
<br/><br/>
 <p align="center"><img src="/Images/Sana-tana.jpg" alt="sana-tana" title="sana-tana" width="438" /></p>
 

 </asp:Content>
