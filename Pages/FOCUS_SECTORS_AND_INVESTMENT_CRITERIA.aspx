﻿<%@ Page Title="Focus Sectors and Investment Criteria | Tana" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true" CodeFile="FOCUS_SECTORS_AND_INVESTMENT_CRITERIA.aspx.cs" Inherits="Pages_FOCUS_SECTORS_AND_INVESTMENT_CRITERIA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>        SetMenu = "About";</script>
    <meta name="description" content="Tana invests between US$20 million and US$75 million to acquire significant minority or control equity positions in established businesses across Africa that operate within the Consumer and Agriculture sectors." />
    <meta name="keywords" content="Consumer Focus, Agriculture Focus, Investment Criteria,Food, beverage and personal care fast moving consumer goods" />
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <span class="breadCrumb">You are Here : <a href="../Default.aspx">Home</a> > About  > Focus Sectors and Investment Criteria</span>
<br />
<br />
  <h1><strong>Focus Sectors and Investment Criteria</strong></h1>
  <p align="justify">Tana invests between US$20 million and US$75 million to acquire significant minority or control equity positions in established businesses.</p>
  
  <h2>Consumer Focus</h2>
  <p align="justify">Companies well positioned to meet the consumption needs of Africa’s young, energetic and growing population are core to Tana’s consumer focus.</p>
  
  <p><strong>Within the consumer sector, Tana focuses its efforts on:</strong></p>
  <ul>
    <li>Food, beverage and personal care fast moving consumer goods</li>
    <li>Retail </li>
    <li>Education </li>
    <li>Healthcare </li>
    <li>Agribusiness</li>
  </ul>
  <p align="justify">We would also consider select opportunities in leisure, manufacturing, retail financial services, media, logistics and business services.</p>

  <!--<p align="center"><br />
  <img src="../Images/agriculture-focus.jpg" alt="Agriculture Focus" title="Agriculture Focus" width="438" height="213" /></p>
   -->
  <h2>Investment Criteria</h2>
  <p align="justify">Tana prefers companies operating in sectors that have the following characteristics:</p>
  <ul>
<li>Large, addressable and rapidly growing market</li>
<li>Minimal public sector dependency</li>
<li>Attractive industry structure with proven, profitable and sustainable business model</li>
<li>Highly scalable business with standardised and repeatable processes</li>
<li>Internal expansion opportunities, as well as potential for add-on acquisitions</li>
<li>Distinct competitive advantage driving strong market position</li>
<li>Talented and visionary entrepreneur/majority shareholder/management team</li>
<li>Clearly identifiable and implementable levers for value creation</li>
<li>Appropriate corporate governance</li>

  </ul>
  <br />
</asp:Content>

