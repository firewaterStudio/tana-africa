<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<title>Tana Africa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>

<h2>
TANA AFRICA CAPITAL
</h2><br><p>
Building enduring businesses
</p>
<br><br>
<div class="wrapper">

<p class="center">
<img src="images/tan_logo.jpg" width="180">
</p>
</div>
<br>
<div class="wrapper">
<h1> TANA AFRICA CAPITAL </h1><p>is an investment company that seeks to acquire significant minority or control equity positions in Africa-based companies operating in Consumer-driven Industries and Agriculture.  The Firm is capitalized with US$300 million from E. Oppenheimer & Son and Temasek Holdings. E Oppenheimer & Son is the investment holding company of the Oppenheimer Family, founders of the global mining company, Anglo American Corporation and managing shareholders of De Beers, the world�s leading diamond company. Temasek Holdings is an investment company that has the Government of Singapore as its sole shareholder. 

<br><br>

Through the medium of capital and strong operational enhancement, Tana seeks to build African business institutions for generations to come.  Tana�s investment approach is differentiated by (i) its ability to deploy capital long-term; (ii) deep and on-the-ground operating experience of its management team; and (iii) clear focus on the food and beverage and personal care fast moving consumer goods, related consumer sectors, and agriculture. 

<br><br>

Like its namesake, Lake Tana, the source of the Blue Nile, Tana Africa Capital aims to serve as a fount of development, leaving an enduring, tangible and positive legacy on the businesses which it supports. 
</p>
</div>
<br>
<div class="wrapper">

    <div id="first"><h1>CONTACT INFORMATION</h1>
<br>
Tana Africa Capital Managers (Pty) Ltd
<br>
6 St Andrew�s Road, Parktown, 2193
<br>
Johannesburg, South Africa 

</div>
    <div id="second">
	<br>
Phone: 	+27 11 274 2130
<br>
Fax:  	+27 11 274 2131
<br>
<a href="mailto: info@tana-africa.com">Email:  	info@tana-africa.com</a>

	</div>
	
<div class="clear">
</div>
</div>
</body>
</html>
