﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DotNetOpenMail;
using System.IO;
using System.Text;
using System.Web.Mail;


/// <summary>
/// Summary description for DataAccessor
/// </summary>
public class DataAccessor
{
    public static bool SendMailHTML(string MailBody, string MailSubject,
    string MailToName, string MailTo,
    string MailCCName, string MailCC,
    string MailFrom, string MailFromEmail,
    string[] strCustomHeaders, string[] strAttachments)
    {
        StringWriter strWriter = new StringWriter();
        EmailMessage emailMessage = new EmailMessage();

        emailMessage.FromAddress = new EmailAddress(MailFromEmail, MailFrom);
        emailMessage.AddToAddress(new EmailAddress(MailTo, MailToName));
        if ((MailCCName != "") && (MailCC != ""))
            emailMessage.AddCcAddress(new EmailAddress(MailCC, MailCCName));

        emailMessage.Subject = MailSubject;

        #region Custom Headers
        emailMessage.AddCustomHeader("SentWith", "DonsMail");
        for (int i = 0; i < strCustomHeaders.Length; i++)
        {
            string[] strSpl = strCustomHeaders[i].Split('|');
            if (strSpl.Length == 2)
                emailMessage.AddCustomHeader(strSpl[0], strSpl[1]);
        }
        emailMessage.AddCustomHeader("SentWithEnd", "DonsMail");
        //emailMessage.AddCustomHeader("Return-Path", "<" + MailFromEmail + ">");
        //emailMessage.AddCustomHeader("Return-Path", ConfigurationManager.AppSettings["SMTPRetPath"]);
        #endregion
        #region Attachments
        for (int i = 0; i < strAttachments.Length; i++)
        {
            if (strAttachments[i] != "")
            {

                FileInfo fi = new FileInfo(strAttachments[i]);
                FileAttachment fa = new FileAttachment(fi);
                emailMessage.AddMixedAttachment(fa);
            }
        }
        #endregion
        emailMessage.EnvelopeFromAddress = new EmailAddress(ConfigurationManager.AppSettings["SMTPRetPath"], MailFrom);
        

        emailMessage.HtmlPart = new HtmlAttachment(MailBody);

        try
        {
            DotNetOpenMail.SmtpServer smtpSrv = new DotNetOpenMail.SmtpServer(ConfigurationManager.AppSettings["SMTPServer"], 587);
            if (true)
            {
                DotNetOpenMail.SmtpAuth.SmtpAuthToken authToken = new DotNetOpenMail.SmtpAuth.SmtpAuthToken(
                    ConfigurationManager.AppSettings["SMTPUsername"], ConfigurationManager.AppSettings["SMTPPass"]);
                smtpSrv.SmtpAuthToken = authToken;
            }
            emailMessage.Send(smtpSrv);
            return true;
        }
        catch (Exception Exp)
        {
            return false;
        }
    }

}
